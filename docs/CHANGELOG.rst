
Changelog
=========

`0.3.3 <https://github.com/saltstack-formulas/golang-formula/compare/v0.3.2...v0.3.3>`_ (2019-07-23)
--------------------------------------------------------------------------------------------------------

Bug Fixes
^^^^^^^^^


* **archives_spec:** check ``sha256sum`` for correct file (\ `99aa62e <https://github.com/saltstack-formulas/golang-formula/commit/99aa62e>`_\ )
* **archives_spec:** remove colon from ``tag:`` (\ `57d445c <https://github.com/saltstack-formulas/golang-formula/commit/57d445c>`_\ )

`0.3.2 <https://github.com/saltstack-formulas/golang-formula/compare/v0.3.1...v0.3.2>`_ (2019-06-28)
--------------------------------------------------------------------------------------------------------

Bug Fixes
^^^^^^^^^


* **\ ``semantic-release``\ :** finalise changes from ``template-formula`` (\ `a38392d <https://github.com/saltstack-formulas/golang-formula/commit/a38392d>`_\ ), closes `#20 <https://github.com/saltstack-formulas/golang-formula/issues/20>`_

`0.3.1 <https://github.com/saltstack-formulas/golang-formula/compare/v0.3.0...v0.3.1>`_ (2019-06-25)
--------------------------------------------------------------------------------------------------------

Bug Fixes
^^^^^^^^^


* **clean:** check for alternative before calling remove (\ `3b75421 <https://github.com/saltstack-formulas/golang-formula/commit/3b75421>`_\ )
* **init:** remove rebase comment (\ `c76d7cd <https://github.com/saltstack-formulas/golang-formula/commit/c76d7cd>`_\ )
* **source_hash:** remove unused 'source_hash' consant (\ `a12c5f7 <https://github.com/saltstack-formulas/golang-formula/commit/a12c5f7>`_\ )

Tests
^^^^^


* **fixes:** fix two kitchen test failures (\ `b01a5d4 <https://github.com/saltstack-formulas/golang-formula/commit/b01a5d4>`_\ )
* **inspec:** add golang archive unittests (\ `6feafa9 <https://github.com/saltstack-formulas/golang-formula/commit/6feafa9>`_\ )
